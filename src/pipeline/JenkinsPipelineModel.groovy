package pipeline

/**
 * Defines the complete pipeline model, that is supported by this DSL.
 */
class JenkinsPipelineModel {

  static String CLEANUP_PARAMETER_NAME = 'REQUEST_CLEANUP'

  def config = new PipelineConfigModel()
  def pipelineSteps = new ArrayList<AbstractStepModel>()
  def vars = [:]

  def config(body) {
    body.delegate = config
    body.resolveStrategy = Closure.DELEGATE_ONLY
    body()
  }

  // The maven build step
  def maven(Closure body) {
    pipelineSteps.add(new MavenStepModel(body, vars))
  }

  // The maven Quarkus build step
  def mavenQuarkus(Closure body) {
    pipelineSteps.add(new MavenQuarkusStepModel(body, vars))
  }

  // The maven legacy build step
  def mavenLegacy(Closure body) {
    pipelineSteps.add(new MavenLegacyStepModel(body, vars))
  }

  // The rust based cargo build step
  def cargo(Closure body) {
    pipelineSteps.add(new CargoStepModel(body, vars))
  }

  // The docker build step
  def docker(Closure body) {
    pipelineSteps.add(new DockerStepModel(body, vars))
  }

  // The helm deployment step
  def helm(Closure body) {
    pipelineSteps.add(new HelmStepModel(body, vars))
  }

  // The katalon test step
  def katalon(Closure body) {
    pipelineSteps.add(new KatalonStepModel(body, vars))
  }

  // The newman test step
  def newman(Closure body) {
    pipelineSteps.add(new NewmanStepModel(body, vars))
  }

  // The angular build step
  def angular(Closure body) {
    pipelineSteps.add(new AngularStepModel(body, vars))
  }

  // The angular elements build step
  def angularElements(Closure body) {
    pipelineSteps.add(new AngularElementsStepModel(body, vars))
  }

  // The riot js build step
  def riotJs(Closure body) {
    pipelineSteps.add(new RiotJsStepModel(body, vars))
  }

  // The flutter js build step
  def flutter(Closure body) {
    pipelineSteps.add(new FlutterStepModel(body, vars))
  }

  // The shell command build step
  def shell(Closure body) {
    pipelineSteps.add(new ShellStepModel(body, vars))
  }

  // The cypress test step
  def cypress(Closure body) {
    pipelineSteps.add(new CypressStepModel(body, vars))
  }

  // The SonarQube analyse step
  def sonarQube(Closure body) {
    pipelineSteps.add(new SonarQubeStepModel(body, vars))
  }

  // The Git Clone step
  def gitClone(Closure body) {
    pipelineSteps.add(new GitCloneStepModel(body, vars))
  }

  /**
   * Executes the build pipeline
   * @param globals A global values map.
   */
  void execute(Map globals) {

    config.execute(globals)

    def steps = globals.steps

    def buildSlaveLabel = "${UUID.randomUUID().toString()}"
    def containerList = createContainers(steps)

    // Assigns build containers to each build step.
    steps.podTemplate(
      label: buildSlaveLabel,
      containers: containerList) {
      steps.node(buildSlaveLabel) {
        GitInfo gitInfo = new GitInfo()
        steps.stage('Git Checkout') {
          def scmvars = steps.checkout([
            $class           : 'GitSCM',
            branches         : globals.scm.branches,
            extensions       : globals.scm.extensions + [[$class: 'LocalBranch'], [$class: 'CleanCheckout']],
            userRemoteConfigs: globals.scm.userRemoteConfigs
          ])

          if (globals.scm.userRemoteConfigs.size() > 0 && globals.scm.userRemoteConfigs[0].credentialsId) {
            globals['gitCredentialsId'] = globals.scm.userRemoteConfigs[0].credentialsId
            globals['gitRemoteUrl'] = globals.scm.userRemoteConfigs[0].url
          }
          if (scmvars.GIT_AUTHOR_NAME && scmvars.GIT_AUTHOR_EMAIL) {
            steps.sh(script: """
							git config user.name '${scmvars.GIT_AUTHOR_NAME}'
							git config user.email '${scmvars.GIT_AUTHOR_EMAIL}'
							""")
          }
          gitInfo.commit = scmvars.GIT_COMMIT
          def gittag = steps.sh(script: "git tag -l --points-at HEAD", returnStdout: true).trim()
          if (gittag) {
            gitInfo.isTag = true
            gitInfo.tag = gittag
            gitInfo.branch = scmvars.GIT_BRANCH
            gitInfo.name = gittag
          } else {
            gitInfo.name = scmvars.GIT_BRANCH
            gitInfo.branch = scmvars.GIT_BRANCH
            gitInfo.isMaster = gitInfo.name == 'master'
          }
          steps.echo "git info: ${gitInfo}"
        }
        globals['gitInfo'] = gitInfo
        vars['GIT_BRANCH'] = gitInfo.branch
        vars['GIT_TAG'] = gitInfo.tag
        vars['GIT_BRANCH_OR_TAG_NAME'] = gitInfo.name
        vars['GIT_COMMIT'] = gitInfo.commit

        // should clean up if it is not on master or a build based on tag and the property to do the clean up is true
        config.shouldCleanUp(!gitInfo.isMaster && !gitInfo.isTag && globals.params.getOrDefault(CLEANUP_PARAMETER_NAME, false))

        // Executes the pipeline step.
        for (step in pipelineSteps) {
          step.execute(config, globals)
          if (globals.stopBuildPipeline) {
            break
          }
        }
        if (config.isCleanUpRequested) {
          steps.stage('Git Cleanup') {
            def cleanUpStep = createCleanUpStep(steps, globals, gitInfo)
            cleanUpStep()
          }
        }
      }
    }
  }

  List createContainers(steps) {
    def containerList = []
    // default jenkins slave image
    containerList.add(steps.containerTemplate(
      name: 'jnlp',
      image: 'jenkins/jnlp-slave:alpine',
      args: '${computer.jnlpmac} ${computer.name}',
      alwaysPullImage: false))

    for (step in pipelineSteps) {
      containerList.add(step.createContainerTemplate(steps))
    }
    containerList
  }

  static createCleanUpStep(steps, globals, gitInfo) {
    // since there are different options to authenticate for a git repository
    // the next step distinguishes based on the URL of the remote
    if (globals.gitRemoteUrl.contains("https://")) {
      return {
        steps.withCredentials([
          steps.usernamePassword(
            credentialsId: globals.gitCredentialsId,
            passwordVariable: 'GIT_PASS',
            usernameVariable: 'GIT_USER')
        ]) {
          // Should it use GIT_ASKPASS ?
          def remoteUrl = globals.gitRemoteUrl.replace("https://", "")
          // both vars are masked by 'withCredentials'
          steps.sh("git push https://\$GIT_USER:\$GIT_PASS@${remoteUrl} --delete ${gitInfo.branch}")
        }
      }
    }

    return {
      steps.sshagent([globals.gitCredentialsId]) {
        steps.sh("git push origin --delete ${gitInfo.branch}")
      }
    }
  }
}
