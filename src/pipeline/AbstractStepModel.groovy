package pipeline

/**
 * Abstract step class.
 * All steps should inherit from this class.
 */
abstract class AbstractStepModel {

  Closure bodyClosure
  Map vars

  Closure beforeClosure
  Closure afterClosure

  /**
   * Constructs a new step.
   * @param bodyClosure The execution body closure.
   * @param vars The global variables.
   */
  AbstractStepModel(Closure bodyClosure, Map vars) {
    this.bodyClosure = bodyClosure
    this.vars = vars
    bodyClosure.delegate = this
    bodyClosure.resolveStrategy = Closure.DELEGATE_ONLY
  }

  /**
   * Sets the before script closure, which can be executed in a specific build step before the execution takes place.
   * The script content can be defined outside of this DSL within the concrete build pipeline.
   * @param beforeClosure The before closure.
   */
  void before(Closure beforeClosure) {
    this.beforeClosure = beforeClosure
  }

  /**
   * Sets the after script closure, which can be executed in a specific build step after the execution has took place.
   * The script content can be defined outside of this DSL within the concrete build pipeline.
   * @param beforeClosure The before closure.
   */
  void after(Closure afterClosure) {
    this.afterClosure = afterClosure
  }

/**
 * Global model variable access and encapsulation function. Returns the variable value for a passed variable name.
 * Throws an Exception if no value could be found.
 * @param name The name of the variable.
 * @return The value of the variable.
 */
  String vars(String name) {
    if (vars[name]) {
      return vars[name]
    }
    throw new Exception("Model variable ${name} is not defined.")
  }

  /**
   * Executes the step by calling an abstract method.
   * @param config The step configuration.
   * @param globals The global variable map.
   */
  void execute(config, Map globals) {
    bodyClosure()
    doExecute(config, globals)
  }

  /**
   * The execution method, which must be implemented by every build step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  abstract void doExecute(config, Map globals)

  /**
   * Runs the before execution script.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void runBeforeScripts(config, Map globals) {
    if (beforeClosure) {
      beforeClosure.delegate = globals.jenkins
      beforeClosure.resolveStrategy = Closure.DELEGATE_ONLY
      beforeClosure()
    }
  }

  /**
   * Runs the after execution script.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void runAfterScripts(config, Map globals) {
    if (afterClosure) {
      afterClosure.delegate = globals.jenkins
      afterClosure.resolveStrategy = Closure.DELEGATE_ONLY
      afterClosure()
    }
  }

  abstract String stepContainerName()

  abstract String stepContainerImageName()

  abstract def createContainerTemplate(steps)
}
