package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute rust based cargo builds.
 * The Cargo.toml should be situated in the root folder.
 * Rust Workspaces are currently not supported.
 */
@InheritConstructors
class CargoStepModel extends AbstractStepModel {

  boolean skipTests = false
  boolean skipStepExecution = false
  boolean appendBranchToVersion = true
  List appendBranchToVersionExceptBranches = ["master"]
  String testOptions = ""
  String buildOptions = ""

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a flag which indicates, if the test should be skipped.
   * @param skipTests True if the test should be skipped. False otherwise.
   */
  void skipTests(boolean skipTests = true) {
    this.skipTests = skipTests
  }

  /**
   * Appends the branch to the version.
   * @param appendBranchToVersion True, if the branch should be appended.
   * @param appendBranchToVersionExceptBranches Branches that should not be appended, even if the feature toggle is
   * set to true.
   */
  void appendBranchToVersion(boolean appendBranchToVersion = true, String... appendBranchToVersionExceptBranches) {
    this.appendBranchToVersion = appendBranchToVersion
    if (appendBranchToVersionExceptBranches.length == 0) {
      this.appendBranchToVersionExceptBranches = ["master"]
    } else {
      this.appendBranchToVersionExceptBranches = appendBranchToVersionExceptBranches as List
    }
  }

  /**
   * Sets additional cargo build options. The options are every time a rust app is build.
   * @param buildOptions The options to set.
   */
  void setBuildOptions(String buildOptions = "") {
    this.buildOptions = buildOptions
  }

  /**
   * Sets additional angular test options. The options are every time a rust app tested.
   * @param testOptions The options to set.
   */
  void setTestOptions(String testOptions = "") {
    this.testOptions = testOptions
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Cargo build")

      def cargoToml = steps.readFile(file: 'Cargo.toml')
      Properties properties = new Properties()
      InputStream is = new ByteArrayInputStream(cargoToml.getBytes())
      properties.load(is)

      def cargoName = properties.getProperty("name")
      if (!cargoName || cargoName.trim().length() == 0) {
        steps.error("Cargo name not set!")
      }
      vars['CARGO_NAME'] = cargoName.substring(1, cargoName.length() - 1)

      def cargoVersion = properties.getProperty("version")
      if (!cargoVersion || cargoVersion.trim().length() == 0) {
        steps.error("Cargo version not set!")
      }
      vars['CARGO_VERSION'] = cargoVersion.substring(1, cargoVersion.length() - 1)

      def finalCargoVersion
      if (globals.gitInfo.isTag || !appendBranchToVersion || (globals.gitInfo.name in this.appendBranchToVersionExceptBranches)) {
        finalCargoVersion = cargoVersion.replaceAll("/['\"]+/g", "")
      } else {
        finalCargoVersion = cargoVersion.replace("-SNAPSHOT", "") + "-" + globals.gitInfo.name.replace("/", "-") + "-SNAPSHOT"
      }

      steps.echo("Building (from Cargo.toml): ${vars['CARGO_NAME']}:${vars['CARGO_VERSION']}")

      globals.currentBuild.description = finalCargoVersion
      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        if (!skipTests) {
          steps.sh("cargo test --release ${testOptions}")
        }
        steps.sh("cargo build --release ${buildOptions}")
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Cargo Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "cargo"
  }

  @Override
  String stepContainerImageName() {
    return "ekidd/rust-musl-builder:1.37.0"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
