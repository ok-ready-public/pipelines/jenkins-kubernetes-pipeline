package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute riot builds.
 */
@InheritConstructors
class RiotJsStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String[] shellCommands = []

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets the NPM commands to execute.
   * @param shellCommands The NPM commands to execute.
   */
  void setShellCommands(String[] shellCommands = []) {
    this.shellCommands = shellCommands
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting riot build")

      def npmBuildCommand = ""

      if (!skipStepExecution && shellCommands.length > 0) {
        shellCommands.each { command ->
          npmBuildCommand += """
             ${command}
          """
        }
      }

      runBeforeScripts(config, globals)
      steps.sh(npmBuildCommand)
      runAfterScripts(config, globals)
    }

    steps.stage('Riot Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "riot"
  }

  @Override
  String stepContainerImageName() {
    return "okready/riot:amd64-4.0.3"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
            name: stepContainerName(),
            image: stepContainerImageName(),
            command: 'cat',
            ttyEnabled: true,
            alwaysPullImage: false
    )
  }
}
