package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute SonarQube analyses and check against quality gates.
 */
@InheritConstructors
class SonarQubeStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  boolean disableQualityGate = false
  boolean ignoreQualityGateResult = false
  boolean enableGitLab = false

  String hostUrl = null
  String apiUrl = null
  String project = null
  String tokenReference = "sonarqube-token"
  String sourceDir = "src"
  String baseDir = "./"
  String binariesDir = "target/classes"
  String testBinariesDir = "target/test-classes"
  String testDir = "src/test"
  String exclusions = "src/main/resources/static/angular/**,**/node_modules/**,src/test/**,**/3rd-party/**,src/main/angular/**/e2e/**,src/main/angular/**/*.spec.ts"
  String librariesDir = "target/dependency"
  String jacocoReportPath = "target/site/jacoco/jacoco.xml"
  String projectVersion = null
  String gitLabProject = null
  String gitLabCommitSha = null
  String gitLabBranch = null
  String typescriptReportPaths = null
  String testInclusions = null
  String testExecutionReportPaths = null
  String junitReportPaths = null
  int sleepBeforeQualityGateExecution = 30000


  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Disables the quality gate.
   * Default: false.
   * @param disableQualityGate True, if the quality gate should be disabled.
   */
  void disableQualityGate(boolean disableQualityGate = true) {
    this.disableQualityGate = disableQualityGate
  }

  /**
   * Ignores the quality gate result.
   * In consequence the pipeline will not stop if the criteria of the gate do not match.
   * Default: false.
   * @param ignoreQualityGateResult True, if the result of the quality gate check should be ignored.
   */
  void ignoreQualityGateResult(boolean ignoreQualityGateResult = true) {
    this.ignoreQualityGateResult = ignoreQualityGateResult
  }

  /**
   * Should be set to true, if the Gitlab integration should be enabled
   * @param gitLabProject Project ID in GitLab or internal id or namespace + name or namespace + path or url http or ssh url or url or web.
   * @param gitLabBranch The GitLab branch.
   * @param gitLabCommitSha The GitLab commit SHA.
   */
  void enableGitlab(gitLabProject, gitLabBranch, gitLabCommitSha) {
    this.enableGitLab = true
    if (!gitLabProject || gitLabProject.trim().length() == 0) {
      steps.error("Please provide a GitLab project URL.")
    }
    if (!gitLabBranch || gitLabBranch.trim().length() == 0) {
      steps.error("Please provide a GitLab commit branch.")
    }
    if (!gitLabCommitSha || gitLabCommitSha.trim().length() == 0) {
      steps.error("Please provide a GitLab commit SHA.")
    }
    this.gitLabProject = gitLabProject
    this.gitLabBranch = gitLabBranch
    this.gitLabCommitSha = gitLabCommitSha
  }

  /**
   * Sets the host url.
   * @param hostUrl The host url.
   */
  void setHostUrl(String hostUrl) {
    this.hostUrl = hostUrl
  }

  /**
   * Sets the project version
   * Default: null.
   * @param hostUrl The project version.
   */
  void setProjectVersion(String projectVersion) {
    this.projectVersion = projectVersion
  }

  /**
   * Sets the api url.
   * @param apiUrl The api url.
   */
  void setApiUrl(String apiUrl) {
    this.apiUrl = apiUrl
  }

  /**
   * Sets the name of the project.
   * @param project The name of the project.
   */
  void setProject(String project) {
    this.project = project
  }

  /**
   * The the name of the token in jenkins.
   * Default value: sonarqube-token
   * @param tokenReference The name of the jenkins token.
   */
  void setTokenReference(String tokenReference) {
    this.tokenReference = tokenReference
  }

  /**
   * Sets the src dir.
   * Default value: src
   * @param sourceDir The source directory.
   */
  void setSourceDir(String sourceDir) {
    this.sourceDir = sourceDir
  }

  /**
   * Sets the base directory.
   * Default value: ./
   * @param baseDir The base directory.
   */
  void setBaseDir(String baseDir) {
    this.baseDir = baseDir
  }

  /**
   * Sets the binaries directory.
   * Default value: target/classes
   * @param binariesDir The binaries directory.
   */
  void setBinariesDir(String binariesDir) {
    this.binariesDir = binariesDir
  }

  /**
   * Sets the test binaries directory.
   * Default value: target/classes
   * @param testBinariesDir The test binaries directory.
   */
  void setTestBinariesDir(String testBinariesDir) {
    this.testBinariesDir = testBinariesDir
  }

  /**
   * Sets the test directory.
   * Default value: src (but in conjunction with test.inclusions)
   * @param testDir The test directory.
   */
  void setTestDir(String testDir) {
    this.testDir = testDir
  }

  /**
   * Sets the libraries directory.
   * Default value: target/dependency
   * @param librariesDir The libraries directory.
   */
  void setLibrariesDir(String librariesDir) {
    this.librariesDir = librariesDir
  }

  /**
   * Sets the excluded files and folders.
   * Default value: Angular compile output, node_modules folder, src/test folder and 3rd-party folder.
   * @param exclusions The excluded files and folders.
   */
  void setExclusions(String exclusions) {
    this.exclusions = exclusions
  }

  /**
   * Sets the inclusion files and folders for tests.
   * Default value: Angular spec files, Java files in src/test
   * @param testInclusions The included files and folders.
   */
  void setTestInclusions(String testInclusions) {
    this.testInclusions = testInclusions
  }

  /**
   * Sets the paths for all reports with test execution information
   * @param testExecutionReportPaths
   */
  void setTestExecutionReportPath(String testExecutionReportPath) {
    this.testExecutionReportPaths = testExecutionReportPath
  }

  /**
   * Sets the paths for all reports with test execution information
   * @param testExecutionReportPaths
   */
  void setTestExecutionReportPaths(String testExecutionReportPaths) {
    this.testExecutionReportPaths = testExecutionReportPaths
  }

  /**
   * Sets the JaCoCo report path .
   * Default value: target/site/jacoco/jacoco.xml
   * @param jacocoReportPath The JaCoCo report path.
   */
  void setJacocoReportPath(String jacocoReportPath) {
    this.jacocoReportPath = jacocoReportPath
  }

  /**
   * Sets the JaCoCo report path .
   * Default value: target/site/jacoco/jacoco.xml
   * @param jacocoReportPath The JaCoCo report path.
   */
  void setJacocoReportPaths(String jacocoReportPaths) {
    this.jacocoReportPath = jacocoReportPaths
  }

  /**
   * Sets the sonar.junit.reportPaths property
   * @param junitReportPaths
   */
  void setJunitReportPath(String junitReportPath) {
    this.junitReportPaths = junitReportPath
  }

  /**
   * Sets the sonar.junit.reportPaths property
   * @param junitReportPaths
   */
  void setJunitReportPaths(String junitReportPaths) {
    this.junitReportPaths = junitReportPaths
  }

  /**
   * Sets the report paths for typescript coverage information
   * No default
   * @param typescriptReportPaths paths to coverage information
   */
  void setTypescriptReportPath(String typescriptReportPath) {
    this.typescriptReportPaths = typescriptReportPath
  }

  /**
   * Sets the report paths for typescript coverage information
   * No default
   * @param typescriptReportPaths paths to coverage information
   */
  void setTypescriptReportPaths(String typescriptReportPaths) {
    this.typescriptReportPaths = typescriptReportPaths
  }
  
   /**
   * Sets the sleep time before quality gate execution.
   * No default
   * @param sleepBeforeQualityGateExecutionInMS sleep time in milliseconds
   */
  void setSleepBeforeQualityGateExecution(int sleepBeforeQualityGateExecutionInMS) {
    this.sleepBeforeQualityGateExecution = sleepBeforeQualityGateExecutionInMS
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      steps.echo("Starting SonarQube scan")

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        if (!hostUrl || hostUrl.trim().length() == 0) {
          steps.error("Host utl not set! Use 'setHostUrl(string url)' to specify the host url.")
        }
        if (!apiUrl || apiUrl.trim().length() == 0) {
          steps.error("API utl not set! Use 'setApiUrl(string url)' to specify the API url.")
        }
        if (!project || project.trim().length() == 0) {
          steps.error("Project name not set! Use 'setProject(string name)' to specify the project name.")
        }
        steps.withCredentials([steps.string(credentialsId: tokenReference, variable: 'TOKEN')]) {
          def sonarQubeCommand = 'sonar-scanner'
          sonarQubeCommand += ' -Dsonar.login="$TOKEN"'
          sonarQubeCommand += " -Dsonar.host.url=${hostUrl}"
          sonarQubeCommand += " -Dsonar.projectKey=${project}"
          sonarQubeCommand += " -Dsonar.projectName=${project}"
          if (sourceDir && sourceDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.sources=${sourceDir}"
          }
          if (baseDir && baseDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.projectBaseDir=${baseDir}"
          }
          if (binariesDir && binariesDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.java.binaries=${binariesDir}"
          }
          if (exclusions && exclusions.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.exclusions=${exclusions}"
          }
          if (typescriptReportPaths && typescriptReportPaths.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.typescript.lcov.reportPaths=${typescriptReportPaths}"
          }
          if (librariesDir && librariesDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.java.libraries=${librariesDir}"
          }
          if (testDir && testDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.tests=${testDir}"
          }
          if (jacocoReportPath && jacocoReportPath.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.coverage.jacoco.xmlReportPaths=${jacocoReportPath}"
          }
          if (testBinariesDir && testBinariesDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.java.test.binaries=${testBinariesDir}"
          }
          if (librariesDir && librariesDir.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.java.test.libraries=${librariesDir}"
          }
          if (testInclusions && testInclusions.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.test.inclusions=${testInclusions}"
          }
          if (testExecutionReportPaths && testExecutionReportPaths.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.testExecutionReportPaths=${testExecutionReportPaths}"
          }
          if (junitReportPaths && junitReportPaths.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.junit.reportPaths=${junitReportPaths}"
          }
          if (projectVersion && projectVersion.trim().length() > 0) {
            sonarQubeCommand += " -Dsonar.projectVersion=${projectVersion}"
          }
          if (enableGitLab) {
            sonarQubeCommand += " -Dsonar.gitlab.project_id=${gitLabProject}"
            sonarQubeCommand += " -Dsonar.gitlab.commit_sha=${gitLabCommitSha}"
            sonarQubeCommand += " -Dsonar.gitlab.ref_name=${gitLabBranch}"
            sonarQubeCommand += " -Dsonar.scm.provider=git"
            sonarQubeCommand += " -Dsonar.scm.disabled=false"
          } else {
            sonarQubeCommand += " -Dsonar.scm.disabled=true"
          }

          if (config.isCleanUpRequested) {
            // delete project
            steps.sh("curl -u \$TOKEN -X POST ${apiUrl}/api/projects/delete?project=${project}")
          } else {
            steps.sh(sonarQubeCommand)
          }
        }
      }

      // no need to check for quality gate in clean up because it should be deleted already
      if (!config.isCleanUpRequested && !disableQualityGate) {
        steps.echo("Waiting for quality gate...")
        sleep(this.sleepBeforeQualityGateExecution)
        steps.echo("Checking SonarQube Quality Gate and got response:")
        steps.withCredentials([steps.string(credentialsId: tokenReference, variable: 'TOKEN')]) {
          String apiCallUrl = apiUrl + "/api/qualitygates/project_status?projectKey=${project}"
          String response = steps.sh(script: 'curl -u "$TOKEN" ' + apiCallUrl, returnStdout: true)
          steps.echo(response)
          if (response.toLowerCase().contains('"status":"error"')) {
            if (ignoreQualityGateResult) {
              steps.echo("FAILED to match SonarQube Quality Gate. Pipeline would shut down but quality gate result is ignored.")
            } else {
              steps.error("FAILED to match SonarQube Quality Gate. Shutting down pipeline.")
            }
          } else {
            steps.echo("SUCCESSFULLY matched SonarQube Quality Gate.")
          }
        }
      } else {
        steps.echo("SonarQube Quality Gate disabled.")
      }

      runAfterScripts(config, globals)
    }

    steps.stage('SonarQube Scan') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "sonarqube"
  }

  @Override
  String stepContainerImageName() {
    return "newtmitch/sonar-scanner:4"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
            name: stepContainerName(),
            image: stepContainerImageName(),
            command: 'cat',
            ttyEnabled: true,
            alwaysPullImage: false
    )
  }
}
