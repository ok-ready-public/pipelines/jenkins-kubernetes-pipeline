package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute katalon end-to-end test.
 */
@InheritConstructors
class KatalonStepModel extends AbstractStepModel {

  String katalonVariables = ""

  boolean skipStepExecution = false

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Adds a katalon variable to overwrite values in katalon test definition.
   * @param key The key of the katalon variable.
   * @param value The boolean value of the jatalon variable.
   */
  void addKatalonVariable(String key, boolean value) {
    addKatalonVariable(key, value ? "true" : "false")
  }

  /**
   * Adds a katalon variable to overwrite values in katalon test definition.
   * @param key The key of the katalon variable.
   * @param value The int value of the jatalon variable.
   */
  void addKatalonVariable(String key, int value) {
    addKatalonVariable(key, "" + value)
  }

  /**
   * Adds a katalon variable to overwrite values in katalon test definition.
   * @param key The key of the katalon variable.
   * @param value The string value of the jatalon variable.
   */
  void addKatalonVariable(String key, String value) {
    katalonVariables += katalonVariables == "" ? "" : " "
    katalonVariables += "-" + key + "=" + "\"" + value + "\""
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Katalon tests")

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        steps.echo("Executing Katalon by using the following variables: ${katalonVariables}")
        steps.sh("katalon-execute.sh -runMode=console -browserType=\"Chrome\" -projectPath=\"/test/katalon/test\" -retry=0 -statusDelay=15 -testSuiteCollectionPath=\"Test Suites/tscDefault\" ${katalonVariables}")
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Katalon Test') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "katalon"
  }

  @Override
  String stepContainerImageName() {
    return "tgrbi/katalon:6.2.1"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
