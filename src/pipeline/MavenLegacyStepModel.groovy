package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute maven legacy builds.
 */
@InheritConstructors
class MavenLegacyStepModel extends AbstractStepModel {

  boolean deploy = false
  boolean existingArtifactMode = false
  boolean skipStepExecution = false
  boolean skipTests = false
  boolean appendBranchToVersion = true
  List appendBranchToVersionExceptBranches = ["master"]
  String extraOpts = ""
  List mavenReleaseBranches = []

  /**
   * Decides, if the the build should be deployed.
   * @param deploy True if the build should be deployed. False otherwise.
   */
  void deploy(boolean deploy = true) {
    this.deploy = deploy
  }

  /**
   * Decides, if only an existing artifact should be downloaded via maven.
   * @param useExistingArtifact True if an existing artifact should be deployed. False otherwise.
   */
  void useExistingArtifact(boolean useExistingArtifact = true) {
    this.existingArtifactMode = useExistingArtifact
  }

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a flag which indicates, if the test should be skipped.
   * @param skipTests True if the test should be skipped. False otherwise.
   */
  void skipTests(boolean skipTests = true) {
    this.skipTests = skipTests
  }

  /**
   * Appends the branch to the version.
   * @param appendBranchToVersion True, if the branch should be appended.
   * @param appendBranchToVersionExceptBranches Branches that should not be appended, even if the feature toggle is
   * set to true.
   */
  void appendBranchToVersion(boolean appendBranchToVersion = true, String... appendBranchToVersionExceptBranches) {
    this.appendBranchToVersion = appendBranchToVersion
    if (appendBranchToVersionExceptBranches.length == 0) {
      this.appendBranchToVersionExceptBranches = ["master"]
    } else {
      this.appendBranchToVersionExceptBranches = appendBranchToVersionExceptBranches as List
    }
  }

  /**
   * Defines options, that should be added to the maven execution command.
   * @param extraOpts The options to add.
   */
  void options(String extraOpts = "") {
    this.extraOpts = extraOpts
  }

  /**
   * Enables release for a passed set of release branch names.
   * @param mavenReleaseBranches The
   */
  void enableReleases(String... mavenReleaseBranches) {
    if (mavenReleaseBranches.length == 0) {
      this.mavenReleaseBranches = ["master"]
    } else {
      this.mavenReleaseBranches = mavenReleaseBranches as List
    }
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps


    def body = {

      def pomInfo = steps.readMavenPom(file: 'pom.xml')
      vars['MAVEN_GROUP'] = pomInfo.groupId
      vars['MAVEN_ARTIFACT'] = pomInfo.artifactId
      vars['MAVEN_VERSION'] = pomInfo.version

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting maven build")

      if ((globals.gitInfo.name in this.mavenReleaseBranches) && !globals.gitInfo.isTag) {
        config.booleanParameter("MAVEN_RELEASE", "Release a new maven version", false)
        config.stringParameter("NEW_MAVEN_VERSION", "Version to use for release", pomInfo.version.replace("-SNAPSHOT", ""))
      }

      if (globals.env.MAVEN_RELEASE && globals.env.MAVEN_RELEASE == "true") {

        def releaseVersion = "${globals.env.NEW_MAVEN_VERSION}"
        def releaseTag = "release-${releaseVersion}"

        def goal = "release:prepare -DpreparationGoals='verify' -DtagNameFormat='${releaseTag}' -DreleaseVersion=${releaseVersion}"

        def mavenCommand = """
					export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
					export DOCKER_HOST=127.0.0.1
					[ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
					mvn -B -DargLine='-Djava.security.egd=file:///dev/urandom' -Dmaven.test.failure.ignore=false -Dmaven.test.skip=${
          skipTests
        } ${extraOpts} ${goal}
				"""

        globals.currentBuild.description = "Prepare release of ${releaseVersion}"

        steps.sh(mavenCommand)
        globals.currentBuild.result = 'NOT_BUILT'
        globals.stopBuildPipeline = true // stop after this step
      } else if (existingArtifactMode) {
        // Downloads already build artifacts via pom.xml from artifact repository
        def goal = "dependency:get"
        def mavenCommand = """
					export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
					export DOCKER_HOST=127.0.0.1
					[ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
					mvn -B -DargLine='-Djava.security.egd=file:///dev/urandom' -DgroupId=${globals.env.groupId} -DartifactId=${
          globals.env.artifactId
        } -Dversion=${
          globals.env.version
        } -Dpackaging=war -Ddest=portal.war -Dmaven.test.failure.ignore=false -Dmaven.test.skip=${
          skipTests
        } ${extraOpts} ${goal}
				"""
        globals.currentBuild.description = "Download *.war of version ${globals.env.version}"
        steps.sh(mavenCommand)
      } else {
        def goal = deploy ? "deploy dependency:copy-dependencies" : "verify dependency:copy-dependencies"

        def mavenCommand = """
					export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
					export DOCKER_HOST=127.0.0.1
					[ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
					mvn -B -DargLine='-Djava.security.egd=file:///dev/urandom' -Dmaven.test.failure.ignore=false -Dmaven.test.skip=${
          skipTests
        } ${extraOpts} ${goal}
				"""

        def mavenVersion
        if (globals.gitInfo.isTag || !appendBranchToVersion || (globals.gitInfo.name in this.appendBranchToVersionExceptBranches)) {
          mavenVersion = pomInfo.version
        } else {
          mavenVersion = pomInfo.version.replace("-SNAPSHOT", "") + "-" + globals.gitInfo.name.replace("/", "-") + "-SNAPSHOT"
          mavenCommand = """
						mvn versions:set -DnewVersion='${mavenVersion}'
						${mavenCommand}
					"""
        }
        globals.currentBuild.description = mavenVersion
        runBeforeScripts(config, globals)
        if (!skipStepExecution) steps.sh(mavenCommand)
        runAfterScripts(config, globals)
      }
    }

    // credentials are used for git -> provide the same credentials with ssh-agent
    if (globals.gitCredentialsId) {
      def innerBody = body
      body = {
        steps.sshagent([globals.gitCredentialsId]) {
          innerBody()
        }
      }
    }

    // if maven settings is configured, wrap this in "withMaven"
    if (config.mavenSettingsId) {
      def innerBody = body
      body = {
        steps.withMaven(mavenSettingsConfig: config.mavenSettingsId) {
          innerBody()
        }
      }
    }
    // if docker registry is configured, wrap this in "docker.withRegistry"
    // also extend the shell command to make the docker registry settings usable
    if (config.dockerRegistry) {
      def innerBody = body
      body = {
        globals.docker.withRegistry("https://${config.dockerRegistry}", config.dockerRegistryCredentialsId) {
          innerBody()
        }
      }
    }
    steps.stage('Maven Legacy Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "maven-legacy"
  }

  @Override
  String stepContainerImageName() {
    return "tgrbi/jenkins-maven:3.6.1-jdk-11-slim"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )

  }
}
