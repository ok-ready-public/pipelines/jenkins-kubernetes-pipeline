package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute shell commands.
 */
@InheritConstructors
class ShellStepModel extends AbstractStepModel {

  String[] shellCommands = null

  /**
   * Shell commands that should be executed. Every object in the passed list is a separate command.
   * Example: setShellCommand("ls -al", "cd dirname", "ls -al")
   * @param shellCommands shell commands to execute
   */
  void setShellCommands(String... shellCommands) {
    this.shellCommands = shellCommands
  }


  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def allCommands = ""
    def steps = globals.steps

    if (shellCommands && shellCommands.length > 0) {
      shellCommands.each { command ->
        steps.echo "shell commmand: ${command}"
        allCommands += """ 
            ${command} ; 
          """
      }
      printf("allCommands=${allCommands}")
    } else steps.echo("no shell commands defined")

    def body = {

      steps.echo("Executing shell commands")

      runBeforeScripts(config, globals)
      steps.sh(allCommands)
      runAfterScripts(config, globals)
    }

    steps.stage('Shell Execution') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "shell"
  }

  @Override
  String stepContainerImageName() {
    return "samepagelabs/zip"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )

  }
}
