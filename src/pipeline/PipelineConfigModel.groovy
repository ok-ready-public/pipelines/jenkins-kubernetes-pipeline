package pipeline

/**
 * Model, which configures the pipeline and is passed between each step.
 */
class PipelineConfigModel {

  String dockerRegistry
  String dockerRegistryCredentialsId
  boolean executed = false
  boolean isCleanUpRequested = false
  Map globals
  List jobParameters = []
  String mavenSettingsId

  /**
   * The docker registry.
   * @param registry The registry.
   * @param credentialsId The id of the credentials - like they are referenced in Jenkins configuration.
   */
  def dockerRegistry(registry, credentialsId = null) {
    dockerRegistry = registry
    dockerRegistryCredentialsId = credentialsId
  }

  /**
   * Defines a reference to the maven settings.
   * @param settingsId The id of the settings - like they are referenced in Jenkins configuration.
   */
  def mavenSettings(settingsId) {
    mavenSettingsId = settingsId
  }

  /**
   * Defines whether the build requested a clean up
   * @param requestCleanup
   * @return
   */
  def shouldCleanUp(requestCleanup) {
    this.isCleanUpRequested = requestCleanup
  }

  /**
   * Adds a parameter with a boolean default value.
   * @param name The name of the parameter.
   * @param description The description of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  def booleanParameter(String name, String description = '', boolean defaultValue = false) {
    jobParameters.add([
      $class      : 'BooleanParameterDefinition',
      name        : name,
      description : description,
      defaultValue: defaultValue
    ])
    updateProperties()
  }

  /**
   * Adds a parameter with a string default value.
   * @param name The name of the parameter.
   * @param description The description of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  def stringParameter(String name, String description = '', String defaultValue = false) {
    jobParameters.add([
      $class      : 'StringParameterDefinition',
      name        : name,
      description : description,
      defaultValue: defaultValue
    ])
    updateProperties()
  }

  /**
   * Does the execution.
   * @param globals The global variables map.
   */
  def execute(Map globals) {
    this.globals = globals
    this.shouldCleanUp(isCleanUpRequested)
    doExecute()
  }

  /**
   * Execution sets some job properties.
   * This is called once when the pipeline is set up. If a step adds/changes some build options later, it's re-executed
   */
  def doExecute() {
    def steps = this.globals.steps

    if (this.jobParameters.isEmpty()) { // avoid the build to be "parameterized" when no params are set
      steps.properties properties: [
        steps.disableConcurrentBuilds(),
        steps.buildDiscarder(steps.logRotator(numToKeepStr: '10')),
        steps.disableResume()
      ]
    } else {
      steps.properties properties: [
        steps.disableConcurrentBuilds(),
        steps.buildDiscarder(steps.logRotator(numToKeepStr: '10')),
        steps.disableResume(),
        steps.parameters(this.jobParameters)
      ]
    }
    this.executed = true
  }

  /**
   * This runs doExecute but only when the properties were already set
   */
  def updateProperties() {
    if (this.executed) {
      doExecute()
    }
  }


}
