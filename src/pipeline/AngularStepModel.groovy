package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute angular build.
 * All angular apps should be located within the same folder.
 */
@InheritConstructors
class AngularStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  boolean skipTests = false
  boolean skipBuild = false
  String testOptions = ""
  String buildOptions = ""
  String angularAppSourcePath = "src/main/angular"
  String[] angularAppsToBuild

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a flag which indicates, if the test should be skipped.
   * @param skipTests True if the test should be skipped. False otherwise.
   */
  void skipTests(boolean skipTests = true) {
    this.skipTests = skipTests
  }

  /**
   * Sets a flag which indicates, if the build should be skipped.
   * @param skipBuild True if the build should be skipped. False otherwise.
   */
  void skipBuild(boolean skipBuild = true) {
    this.skipBuild = skipBuild
  }

  /**
   * Sets additional angular build options. The options are every time an app is build.
   * @param buildOptions The options to set.
   */
  void setBuildOptions(String buildOptions = "") {
    this.buildOptions = buildOptions
  }

  /**
   * Sets additional angular test options. The options are every time an app gets tested.
   * @param testOptions The options to set.
   */
  void setTestOptions(String testOptions = "") {
    this.testOptions = testOptions
  }

  /**
   * Sets the path to the folder, where the angular apps are located.
   * By default, the folder is set to: 'src/main/angular'
   * @param angularAppSourcePath The path to the folder, where the angular apps are located.
   */
  void setAngularAppSourcePath(String angularAppSourcePath = "") {
    this.angularAppSourcePath = angularAppSourcePath
  }

  /**
   * Set the angular apps, that should be build in this step.
   * If the appSourcePath is set to 'src/main/angular', and this array is passed ["app-v1", "app-v2",
   * "other-app-v1"] the following apps are build: 'src/main/angular/app-v1', 'src/main/angular/app-v2' and
   * 'src/main/angular/other-app-v1'.
   * @param angularAppsToBuild The angular apps to build.
   */
  void setAngularAppsToBuild(String... angularAppsToBuild) {
    this.angularAppsToBuild = angularAppsToBuild
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Angular build")

      def angularBuildCommand = ""

      if (angularAppsToBuild && angularAppsToBuild.length > 0 && !skipStepExecution) {
        angularBuildCommand = """
          cd ${angularAppSourcePath && angularAppSourcePath.length() > 0 ? angularAppSourcePath : '.'}
        """
        angularAppsToBuild.each { appName ->
          steps.echo("Adding app: ${angularAppSourcePath}/${appName} to build")
          angularBuildCommand += """
            cd ${appName && appName.length() > 0 ? appName : '.'}
            npm i patch-package
            ${skipTests ? '' : 'ng test --watch=false ' + testOptions}
            ${skipBuild ? '' : 'ng build --prod ' + buildOptions}
            cd ${appName && appName.length() > 0 ? '..' : '.'}
          """
        }
      } else {
        steps.echo("Skipping step execution: No Angular Apps build.")
      }

      runBeforeScripts(config, globals)
      if (!skipStepExecution && angularBuildCommand.length() > 0) {
        steps.sh(angularBuildCommand)
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Angular Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "angular"
  }

  @Override
  String stepContainerImageName() {
    return "trion/ng-cli-karma:8.3.14"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
