package pipeline


import groovy.transform.InheritConstructors

/**
 * Build step to execute postman integration test.
 */
@InheritConstructors
class NewmanStepModel extends AbstractStepModel {

  private static int SLEEP_BEFORE_EXECUTION_IN_MS = 30000

  boolean skipStepExecution = false
  int repeats = 1
  boolean bail = true
  int delayRequest = 10
  int timeoutRequest = 25000
  String newmanTestSourcePath = "deployment/test/newman/test/"
  String[] newmanTestsToRun = []
  String newmanVariables = "--global-var"

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Defines, how often the tests should be repeated.
   * @param repeats The number of repeats.
   */
  void repeats(int repeats = 1) {
    this.repeats = repeats
  }

  /**
   * Defines, how much time in milli seconds should pass between two requests.
   * @param delayRequest The time in milli seconds.
   */
  void delayRequest(int delayRequest = 10) {
    this.delayRequest = delayRequest
  }

  /**
   * Defines, how long in milli seconds the test should wait for a response.
   * @param timeoutRequest The wait time in milli seconds for each response.
   */
  void timeoutRequest(int timeoutRequest = 25000) {
    this.timeoutRequest = timeoutRequest
  }

  /**
   * Sets the path to the folder, where the newman test are located.
   * By default, the folder is set to: 'deployment/test/newman/test/'
   * @param newmanTestSourcePath The path to the folder, where the newman test are located.
   */
  void setNewmanTestSourcePath(String newmanTestSourcePath = "") {
    this.newmanTestSourcePath = newmanTestSourcePath
  }

  /**
   * Set the newman test, that should be run in this step.
   * If the newmanTestSourcePath is set to 'deployment/test/newman/test/', and this array is passed ["test1.json",
   * "test2.json"] the following tests are run: 'deployment/test/newman/test/test1.json' and
   * 'deployment/test/newman/test/test2.json'
   * @param newmanTestsToRun The newman tests to run.
   */
  void setNewmanTestsToRun(String... newmanTestsToRun) {
    this.newmanTestsToRun = newmanTestsToRun
  }

  /**
   * If set to true, the test is aborted, if a request fails.
   * @param bail Immediately stops the test, if a request fails.
   */
  void bail(boolean bail = true) {
    this.bail = bail
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The boolean value of the variable.
   */
  void addNewmanVariable(String key, boolean value) {
    addNewmanVariable(key, value ? "true" : "false")
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The int value of the variable.
   */
  void addNewmanVariable(String key, int value) {
    addNewmanVariable(key, "" + value)
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The string value of the variable.
   */
  void addNewmanVariable(String key, String value) {
    newmanVariables += newmanVariables == "--global-var" ? " " : " --global-var "
    newmanVariables += key + "=" + value
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps
    String bailValue = bail ? "--bail" : ""
    newmanVariables = newmanVariables == "--global-var" ? "" : newmanVariables

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting newman tests")

      sleep(SLEEP_BEFORE_EXECUTION_IN_MS)
      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        if (newmanTestsToRun && newmanTestsToRun.length > 0) {
          // use every file given (kept for backwards compatibility)
          newmanTestsToRun.each { newmanTest ->
            steps.sh("newman run ${newmanTestSourcePath}${newmanTest} -n ${repeats} ${bailValue} --delay-request ${delayRequest}  --timeout-request ${timeoutRequest} ${newmanVariables}")
          }
        } else {
          // traverse through all JSON-files in test source folder
          def files = steps.findFiles(glob: "${newmanTestSourcePath}/*.json")
          for (file in files) {
            steps.sh("newman run ${file} -n ${repeats} ${bailValue} --delay-request ${delayRequest} --timeout-request ${timeoutRequest} ${newmanVariables}")
          }
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Newman Test') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "newman"
  }

  @Override
  String stepContainerImageName() {
    return "okready/newman:4.5.1"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
