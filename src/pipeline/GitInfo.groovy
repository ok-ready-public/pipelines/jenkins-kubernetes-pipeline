package pipeline

/**
 * Git info variable holder class.
 */
class GitInfo {
  String name
  String commit
  String tag
  String branch
  boolean isTag
  boolean isMaster

  /**
   * Returns all containing git information as string.
   * @return A string with all git information.
   */
  String toString() {
    return "GitInfo[name=${name},tag=${isTag},master=${isMaster},commit=${commit},tag=${tag},branch=${branch}]"
  }
}
