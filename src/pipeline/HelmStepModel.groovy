package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to deploy docker images to a kubernetes cluster via helm and tiller.
 */
@InheritConstructors
class HelmStepModel extends AbstractStepModel {

  boolean debug = false
  boolean skipStepExecution = false
  String chartFolderName = null
  String namespace = null
  String releaseName = null
  String helmVariables = "--set"
  String buildDescription = null

  /**
   * Decided if the helm debug mode should be enabled.
   * @param debug True, if the helm debug mode should be enabled. False otherwise.
   */
  void debug(boolean debug = true) {
    this.debug = debug
  }

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets the name of the chart folder from where the helm chart should be loaded. Supports paths.
   * @param chartFolderName The name of the chart folder.
   */
  void chartFolderName(String chartFolderName) {
    this.chartFolderName = chartFolderName
  }

  /**
   * Sets the kubernetes namespace of the deployment.
   * @param namespace The kubernetes namespace.
   */
  void namespace(String namespace) {
    this.namespace = namespace
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * @param key The key of the helm variable.
   * @param value The boolean value of the helm variable.
   */
  void addHelmVariable(String key, boolean value) {
    addHelmVariable(key, value ? "true" : "false")
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * @param key The key of the helm variable.
   * @param value The int value of the helm variable.
   */
  void addHelmVariable(String key, int value) {
    addHelmVariable(key, "" + value)
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * @param key The key of the helm variable.
   * @param value The string value of the helm variable.
   */
  void addHelmVariable(String key, String value) {
    helmVariables += helmVariables != "--set" ? "," : " "
    helmVariables += key + "=" + value
  }

  /**
   * Sets the release name of the helm deployment.
   * @param releaseName The name of the helm deployment.
   */
  void releaseName(String releaseName) {
    if (releaseName != null) {
      releaseName = releaseName.replace("/", "-")
    }
    this.releaseName = releaseName
  }

  /**
   * Sets a build description.
   * @param buildDescription The build description.
   */
  void buildDescription(String buildDescription) {
    this.buildDescription = buildDescription
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def chartFolderName = this.chartFolderName

    def debugParameter = "--debug"


    if (!chartFolderName || chartFolderName.trim().length() == 0) {
      steps.error("Chart folder name is not set!")
    }

    if (namespace == null || namespace.trim().length() == 0) {
      namespace = "default"
      steps.echo("Name space is not set. Using '${namespace}'")
    }

    if (!releaseName) {
      releaseName(globals.gitInfo.name)
      steps.echo("Release name  is not set. Using '${releaseName}' derived from git tag or branch")
    }

    if (!debug) {
      debugParameter = ""
    }

    if (vars != null) {
      addHelmVariable("maven.groupId", (vars['MAVEN_GROUP'] != null ? vars['MAVEN_GROUP'] : "n/a") as String)
      addHelmVariable("maven.artifactId", (vars['MAVEN_ARTIFACT'] != null ? vars['MAVEN_ARTIFACT'] : "n/a") as String)
      addHelmVariable("maven.artifactVersion", (vars['MAVEN_VERSION'] != null ? vars['MAVEN_VERSION'] : "n/a") as
        String)
      addHelmVariable("cargo.name", (vars['CARGO_NAME'] != null ? vars['CARGO_NAME'] : "n/a") as String)
      addHelmVariable("cargo.version", (vars['CARGO_VERSION'] != null ? vars['CARGO_VERSION'] : "n/a") as String)
    }

    if (globals.gitInfo != null && globals.gitInfo.commit != null) {
      addHelmVariable("git.commit", "${globals.gitInfo.commit.substring(0, 8)}-${globals.gitInfo.commit}")
    }

    helmVariables = helmVariables == "--set" ? "" : helmVariables

    def body = {

      steps.echo("Starting Kubernetes deployment")

      runBeforeScripts(config, globals)
      if (config.isCleanUpRequested) {
        steps.echo("Undeploying release ${releaseName} from namespace '${namespace}'")
        // delete the current release and but ignore any error (e.g. release not available any more)
        steps.sh("helm delete --purge '${releaseName}' || true")
      } else {
        if (!skipStepExecution) {
          steps.echo("Deploying charts from folder '${chartFolderName}' as ${releaseName} to namespace '${namespace}'")
          steps.sh("helm upgrade --wait --timeout 600 --install --force ${debugParameter} ${helmVariables} --namespace '${namespace}' '${releaseName}' '${chartFolderName}' ")
        }
        if (buildDescription != null) {
          globals.currentBuild.description = buildDescription
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Kubernetes Deployment') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "helm"
  }

  @Override
  String stepContainerImageName() {
    return "alpine/helm:2.16.0"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
