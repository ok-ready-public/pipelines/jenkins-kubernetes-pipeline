package pipeline

import groovy.transform.InheritConstructors

/**
 * Clones git repositories.
 */
@InheritConstructors
class GitCloneStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String sourceCredentialReference = ""
  String targetCredentialReference = ""
  String[] sourceRepositoryList
  String[] targetRepositoryList

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a list of source repositories. For example "https://github.com/username/source0.git", "https://github
   * .com/username/source1.git". The repositories must be reference by using the https protocol.
   * @param sourceRepositoryList The list of source repositories.
   */
  void setSourceRepositoryList(String... sourceRepositoryList) {
    this.sourceRepositoryList = sourceRepositoryList
  }

  /**
   * Sets a list of target repositories. For example "https://github.com/username/target0.git", "https://github
   * .com/username/target1.git". The repositories must be reference by using the https protocol.
   * @param targetRepositoryList The list of target repositories.
   */
  void setTargetRepositoryList(String... targetRepositoryList) {
    this.targetRepositoryList = targetRepositoryList
  }

  /**
   * Sets the reference to the Jenkins username/password credential which is used to access the source repository.
   * @param sourceCredentialReference the Jenkins username/password credential which is used to access the source
   * repository.
   */
  void setSourceCredentialReference(String sourceCredentialReference) {
    this.sourceCredentialReference = sourceCredentialReference
  }

  /**
   * Sets the reference to the Jenkins username/password credential which is used to access the target repository.
   * @param targetCredentialReference the Jenkins username/password credential which is used to access the target
   * repository.
   */
  void setTargetCredentialReference(String targetCredentialReference) {
    this.targetCredentialReference = targetCredentialReference
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Git Clone step")

      def gitCloneCommand = ""

      if (sourceCredentialReference == null || sourceCredentialReference.length() == 0) {
        steps.error("Please provide a source credential reference!")
      }

      if (targetCredentialReference == null || targetCredentialReference.length() == 0) {
        steps.error("Please provide a target credential reference!")
      }

      if (targetRepositoryList.length != sourceRepositoryList.length) {
        steps.error("Number of source repositories does not match number of target repositories!")
      }

      runBeforeScripts(config, globals)
      def noSourceCredentials = false;
      if (sourceRepositoryList.length > 0 && !skipStepExecution) {
        steps.withCredentials([steps.usernameColonPassword(credentialsId: sourceCredentialReference, variable: 'SOURCEUSERPASS')]) {
          steps.withCredentials([steps.usernameColonPassword(credentialsId: targetCredentialReference, variable: 'TARGETUSERPASS')
          ]) {
            def repositoryNumber = 0
            sourceRepositoryList.each { sourceRepository ->
              if (!sourceRepository.startsWith("https://")) {
                steps.error("Authentication not supported. ${sourceRepository}")
              }
              if (!targetRepositoryList[repositoryNumber].startsWith("https://")) {
                steps.error("Authentication not supported. ${targetRepositoryList[repositoryNumber]}")
              }
              if (sourceRepository.endsWith('!')) {
                noSourceCredentials = true
                sourceRepository = sourceRepository.replace('!', '')
              } else {
                noSourceCredentials = false
              }
              steps.echo("Cloning source repository: ${sourceRepository}")
              if (noSourceCredentials) {
                steps.sh """
                git clone --bare ${sourceRepository} repository${repositoryNumber}
                """
              } else {
                steps.sh """
                  git clone --bare https://\$SOURCEUSERPASS@${sourceRepository.replace("https://", "")} repository${
                  repositoryNumber
                }
                """
              }
              steps.sh """
              cd repository${repositoryNumber}
              git remote add --mirror=fetch second https://\$TARGETUSERPASS@${
                targetRepositoryList[repositoryNumber].replace("https://", "")
              }
              git fetch origin --tags
              git push second --all --force
              git push second --tags --force
              cd ..
            """
              repositoryNumber++
            }
          }
        }
        runAfterScripts(config, globals)
      } else {
        steps.echo("Skipping step execution: No Git repositories cloned.")
      }
    }

    steps.stage('Git Clone') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "git-clone"
  }

  @Override
  String stepContainerImageName() {
    return "alpine/git:1.0.7"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
