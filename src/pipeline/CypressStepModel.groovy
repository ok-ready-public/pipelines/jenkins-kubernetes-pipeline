package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute postman cypress test.
 */
@InheritConstructors
class CypressStepModel extends AbstractStepModel {

  private static int SLEEP_BEFORE_EXECUTION_IN_MS = 5000

  boolean skipStepExecution = false
  String cypressTestSourcePath = "deployment/test/cypress"
  String cypressConfigVariables = "--config"

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets the path to the folder, where the cypress test are located.
   * By default, the folder is set to: 'deployment/test/cypress'
   * @param cypressTestSourcePath The path to the folder, where the cypress test are located.
   */
  void setCypressTestSourcePath(String cypressTestSourcePath = "") {
    this.cypressTestSourcePath = cypressTestSourcePath
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The boolean value of the variable.
   */
  void addCypressVariable(String key, boolean value) {
    addCypressVariable(key, value ? "true" : "false")
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The int value of the variable.
   */
  void addCypressVariable(String key, int value) {
    addCypressVariable(key, "" + value)
  }

  /**
   * Adds a variable that is appended to the test execution.
   * @param key The key of the variable.
   * @param value The string value of the variable.
   */
  void addCypressVariable(String key, String value) {
    cypressConfigVariables += cypressConfigVariables == "--config" ? " " : " --config"
    cypressConfigVariables += key + "=" + value
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps
    cypressConfigVariables = cypressConfigVariables == "--config" ? "" : cypressConfigVariables

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting cypress tests")

      sleep(SLEEP_BEFORE_EXECUTION_IN_MS)
      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        steps.sh("cypress run --project ${cypressTestSourcePath} ${cypressConfigVariables}")
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Cypress Test') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "cypress"
  }

  @Override
  String stepContainerImageName() {
    return "cypress/included:4.3.0"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
