package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute docker builds and docker tag relabeling.
 */
@InheritConstructors
class DockerStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String imageName = null
  String[] imagesToPull
  String renameFromTag = null
  String renameToTag = null
  ArrayList<String> dockerTags = new ArrayList<>()
  String executionDirectory = null

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets name name of the docker image.
   * @param imageName The name of the docker image.
   */
  void dockerImageName(String imageName) {
    this.imageName = imageName
  }

  /**
   * Adds a tag, which is used to tag the build docker image.
   * @param tagToAdd The tag to add.
   */
  void addDockerTag(String tagToAdd) {
    if (tagToAdd != null) {
      tagToAdd = tagToAdd.replace("/", "-")
    }
    dockerTags.push(tagToAdd)
  }

  /**
   * Pulls an image with a given tag and pushes it with the new tag.
   * The relabeling is done on step execution.
   * @param renameFromTag The old tag.
   * @param renameToTag The new tag
   */
  void dockerReTag(String renameFromTag, String renameToTag) {
    if (renameFromTag == null || renameFromTag.length() == 0) {
      steps.error("Docker rename action triggered but 'renameFromTag' not set!")
    }
    if (renameToTag == null || renameToTag.length() == 0) {
      steps.error("Docker rename action triggered but 'renameToTag' not set!")
    }
    this.renameFromTag = renameFromTag
    this.renameToTag = renameToTag
  }

  /**
   * Pulls one or more images.
   * @param imagesToPull The full qualified image reference to pull.
   */
  void dockerPullPublicImages(String... imagesToPull) {
    this.imagesToPull = imagesToPull
  }

  /**
   * Sets the directory for the docker step execution.
   * If no directory is set, all docker actions are executed in root
   * directory of the repository.
   * @param directory The directory.
   */
  void dir(String directory) {
    this.executionDirectory = directory
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def imageName = this.imageName
    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Docker build")

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        if(imagesToPull && imagesToPull.length > 0){
          imagesToPull.each { imageToPull ->
            steps.echo("Pulling docker image from ${imageToPull}")
            steps.sh("docker pull ${imageToPull}")
          }
        }
        if (!imageName && !imagesToPull) {
          steps.error("Docker imageName is not set!")
        }
        if (dockerTags.size() > 0) {
          steps.echo("Building docker image ${String.join("-t ${imageName}:", dockerTags)}")
          steps.sh("docker build -t ${imageName}:${String.join(" -t ${imageName}:", dockerTags)} .")
          for (String tag in dockerTags) {
            steps.echo("Pushing docker image '${imageName}' with tag '${tag}'")
            steps.sh("docker push ${imageName}:${tag}")
          }
        }
        if (renameFromTag && renameToTag) {
          steps.echo("Renaming docker tag from ${imageName}:${renameFromTag} to ${imageName}:${renameToTag}")
          steps.echo("Preparing rename operation by pulling:  ${imageName}:${renameFromTag}")
          steps.sh("docker pull ${imageName}:${renameFromTag}")
          steps.sh("docker tag ${imageName}:${renameFromTag} ${imageName}:${renameToTag}")
          steps.sh("docker push ${imageName}:${renameToTag}")
        }
      }
      runAfterScripts(config, globals)
    }

    // Changes the execution directory before step execution.
    if (executionDirectory) {
      def innerBody = body
      body = {
        steps.dir(executionDirectory) {
          innerBody()
        }
      }
    }

    // if docker registry is configured, wrap this in "docker.withRegistry"
    // and add the registry to the image name
    if (config.dockerRegistry) {
      imageName = "${config.dockerRegistry}/${imageName}"
      def innerBody = body
      body = {
        globals.docker.withRegistry("https://${config.dockerRegistry}", config.dockerRegistryCredentialsId) {
          innerBody()
        }
      }
    }
    steps.stage('Docker Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "docker"
  }

  @Override
  String stepContainerImageName() {
    return "docker:stable-dind"
  }

  @Override
  def createContainerTemplate(steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(),
      privileged: true,
      alwaysPullImage: false)
  }
}
