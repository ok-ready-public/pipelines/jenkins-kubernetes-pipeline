#!/usr/bin/env groovy

/**
 * Defines the global jenkins pipeline model.
 * @param body The body that should be executed.
 */
def call(body) {
  try {
    originalOwner = body.owner
    model = new pipeline.JenkinsPipelineModel()
    body.delegate = model
    body.resolveStrategy = Closure.DELEGATE_ONLY
    body()
    Map globals = [
      jenkins          : originalOwner,
      env              : env,
      steps            : steps,
      currentBuild     : currentBuild,
      scm              : scm,
      docker           : docker,
      params           : params,
      stopBuildPipeline: false
    ]
    model.execute(globals)
  } catch (err) {
    try {
      rocketSend "Build failed: ${err} - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
      currentBuild.result = 'FAILURE'
      throw err
    } catch (sendError) {
    }
  }


}
